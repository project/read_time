<?php

/**
 * @file
 * Install, update and uninstall functions for the Read Time module.
 */

use Drupal\node\Entity\NodeType;

/**
 * Implements hook_schema().
 */
function read_time_schema() {
  return [
    'read_time' => [
      'description' => 'The calculated read times of nodes.',
      'fields' => [
        'nid' => [
          'description' => 'The {node}.nid of the node.',
          'type' => 'int',
          'not null' => TRUE,
          'unsigned' => TRUE,
        ],
        'read_time' => [
          'description' => 'The calculated and formatted read time of the node.',
          'type' => 'varchar',
          'not null' => TRUE,
          'default' => '',
          'length' => 255,
        ],
      ],
      'primary key' => ['nid'],
    ],
  ];
}

/**
 * Implements hook_uninstall().
 */
function read_time_uninstall() {
  foreach (node_type_get_names() as $bundle => $label) {
    /** @var \Drupal\node\NodeTypeInterface $type */
    $type = NodeType::load($bundle);
    $type->unsetThirdPartySetting('read_time', 'read_time_enable');
    $type->unsetThirdPartySetting('read_time', 'read_time_fields');
    $type->unsetThirdPartySetting('read_time', 'read_time_wpm');
    $type->unsetThirdPartySetting('read_time', 'read_time_format');
    $type->unsetThirdPartySetting('read_time', 'read_time_display');
  }
}
